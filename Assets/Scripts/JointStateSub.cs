using System.Collections.Generic;
using UnityEngine;
using Unity.Robotics.ROSTCPConnector;
using RosMessageTypes.Sensor;
using Unity.Jobs;

public class JointStateSub : MonoBehaviour
{
    public ArticulationBody[] articulationBodies;
    public string topicName = "/joint_states";
    public int jointLength = 9;
    private ROSConnection _ros;

    // Start is called before the first frame update
    private void Start()
    {
        _ros = ROSConnection.GetOrCreateInstance();
        _ros.Subscribe<JointStateMsg>(topicName, Cb);
    }

    // Update is called once per frame
    private void Cb(JointStateMsg msg)
    {
        /* 
        The order of the joints in the /joint_states is not in right order (this is a limitation of ros_control)
        Desired order: x_slide -> y_slide -> z_slide -> joint1 -> joint2 -> joint3 -> joint4 -> joint5 -> joint6
        Actual order: x_slide -> z_slide -> joint2 -> joint3 -> joint1 -> joint4 -> y_slide -> joint5 -> joint6
         */
        var jointValues = new List<double> { 0, msg.position[0], msg.position[6], msg.position[1], msg.position[4], msg.position[2], msg.position[3],msg.position[5],msg.position[7],msg.position[8] };
        for (var i = 4; i < jointLength ; i++)
        {
            var joint1XDrive = articulationBodies[i].xDrive;
            joint1XDrive.target = (float)jointValues[i] * Mathf.Rad2Deg;
            articulationBodies[i].xDrive = joint1XDrive;
        }
        for (var j = 0; j < jointLength - 5; j++)
        {
            var joint1XDrive = articulationBodies[j].xDrive;
            joint1XDrive.target = (float)jointValues[j];
            articulationBodies[j].xDrive = joint1XDrive;
        }
    }
}
